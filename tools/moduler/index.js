
// kye

var fs = require("fs")
var path = require("path")

module.exports = function(baseDir, opts) {

	// parse opts
	if(typeof opts             !== "object" ) opts             = {}
	if(typeof opts.exclude     !== "object" ) opts.exclude     = []
	if(typeof opts.nameOnly    !== "boolean") opts.nameOnly    = false
	if(typeof opts.startingObj !== "object" ) opts.startingObj = {}

	// parse each dir
	var modules = fs.readdirSync(baseDir).reduce(function(obj, name) {

		// get stats
		var stats = fs.statSync(path.join(baseDir, name))

		if(

			// ensure is directory
			stats.isDirectory() &&

			// ensure not excluded per opts
			opts.exclude.indexOf(name) === -1

		) {

			obj[name] = require(path.join(baseDir, name))

		}

		return obj

	}, opts.startingObj)

	if(opts.nameOnly) return modules.keys()
	else return modules

}
